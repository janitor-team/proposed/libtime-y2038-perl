libtime-y2038-perl (20100403-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 29 Aug 2022 22:08:41 +0100

libtime-y2038-perl (20100403-6) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
    (Closes: #978453)
  * Bump debhelper-compat to 13.
  * Update years of packaging copyright.
  * Enable all hardening flags in debian/rules.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Dec 2020 22:40:53 +0100

libtime-y2038-perl (20100403-5) unstable; urgency=medium

  * Team upload.
  * Set TZ=UTC in debian/rules to make the build reproducible.

 -- Niko Tyni <ntyni@debian.org>  Wed, 26 Aug 2015 18:51:33 +0300

libtime-y2038-perl (20100403-4) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * debian/control: update Module::Build dependency.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable.

 -- Niko Tyni <ntyni@debian.org>  Wed, 27 May 2015 23:00:14 +0300

libtime-y2038-perl (20100403-3) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Bump debian/copyright format to 1.0
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compatibility to 9

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 09 Feb 2013 14:35:56 +0100

libtime-y2038-perl (20100403-2) unstable; urgency=low

  * Change build dependency from "perl (>= 5.12) | libmodule-build-perl (>=
    0.36)" to "libmodule-build-perl (>= 0.360000)" until either perl 5.12
    enters unstable or sbuild changes its handling of alternative
    dependencies; thanks to Sebastian Andrzej Siewior for the bug report
    (closes: #583351).
  * Remove ancient versions from perl (build) dependency.
  * Improve short description.
  * Add /me to Uploaders.
  * Convert to source format 3.0 (quilt).
  * Add years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 27 May 2010 17:39:16 +0200

libtime-y2038-perl (20100403-1) unstable; urgency=low

  * New upstream release
  * lintian: Build-Depends: perl before libmodule-build-perl

 -- Ivan Kohler <ivan-debian@420.am>  Sun, 25 Apr 2010 20:55:55 -0700

libtime-y2038-perl (20100225-2) unstable; urgency=low

  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza). Changed: Maintainer set to Debian Perl Group
    <pkg-perl-maintainers@lists.alioth.debian.org> (was: Ivan Kohler
    <ivan-debian@420.am>); Ivan Kohler <ivan-debian@420.am> moved to
    Uploaders.
  * Add B-D-I on libmodule-build-perl (>= 0.36) | perl (>= 5.12)
    (closes: Bug#576994)
  * Using the required Module::Build version should prevent errors about
    time64_config.h too (closes: Bug#577331)

 -- Ivan Kohler <ivan-debian@420.am>  Sun, 18 Apr 2010 09:09:05 -0700

libtime-y2038-perl (20100225-1) unstable; urgency=low

  * Initial Release (closes: Bug#575974).

 -- Ivan Kohler <ivan-debian@420.am>  Tue, 30 Mar 2010 19:15:32 -0700
